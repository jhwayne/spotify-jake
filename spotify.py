import urllib
import urllib2
import json

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)


class Spotify:
	
	baseurl = 'https://ws.spotify.com/search/1/'
	format = ''

	def __init__(self):
		pass

	def do_search(self, format, search):
		music_request = self.baseurl+format+".json?q="+search
		#print music_request
		try:
			result = urllib2.urlopen(music_request)
			music_json_str = result.read()
			#print music_json_str

			music_data=json.loads(music_json_str)
		except Exception, e:
			print "******** ERROR ********"
			print e
			print "***********************"
			return []

		inputs=str(format)+"s"

		n_out=len(music_data[inputs])

		x=range(n_out)
		results=[]
		for number in range(n_out):
			name=music_data[inputs][number]["name"]
			results.append(name)

		return results



	def get_user_choice(self, prompt, choices):
		print prompt
		for n,c in enumerate(choices):
			print "%d - %s" % (n, c)
		num = raw_input("Pick a number: ")
		try:
			num = int(num)
			choice = choices[num]
		except Exception:
			print "Bad Choice, Try Again"
			return self.get_user_choice(prompt,choices)
		return choice

	def search(self, formats=['track', 'album', 'artist'], format=None, search=None):
		if not format:
			format = self.get_user_choice("What are you searching for?",formats)
		if not search:
			search=raw_input("Search any "+format.capitalize()+":")
		search=search.replace(" ","+")

		results = self.do_search(format, search)

		choice = self.get_user_choice("Pick a result", results)

		formats.remove(format)
		self.search(formats=formats, search=choice)
